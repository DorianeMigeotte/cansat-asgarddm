/*
   Test program for cansat_debug library
XXXXXXX
XXXXXXX
   Simulates tracing in 2 different source files and 2 external libraries.
   See On-board Sofware Design Document for test instructions.
   
*/

#include "tst_cansat_config.h"
#include "tst_cansat_src.h"

#include "test_lib1.h"
#include "test_lib2.h"
#include "test_simpleLib.h"

// ------------------------- MAIN FUNCTIONS ---------------------------------
void setup() {
  
  DINIT(9600); // Init debugging
   // All following lines are noop if DEBUG is not defined.
  DFREE_RAM(DBG_SETUP);
  pinMode(LED_BUILTIN, OUTPUT);
  DPRINTSLN(DBG_SETUP, "setup OK");
  //NB: do not use the F macro: it is automatically used inside the cansat_debug library
}

void loop() {
  DPRINTSLN(DBG_LOOP, "--------------------");
  DPRINTSLN(DBG_LOOP, "**loop in **");

  DPRINTS(DBG_LOOP, "   Printing float (531.5), without and with a carriage return after the numeric value: ");
  DPRINT(DBG_LOOP, 531.5);
  DPRINTS(DBG_LOOP, " ");
  DPRINTLN(DBG_LOOP, 531.5);

#ifdef DO_SOMETHING_LOCAL
  doSomething();  // Call function in other source file within same sketch
#endif
#ifdef CALL_LIBRARIES
  dummySimpleLibFunction(); // Call function from single file library
  dummyLibFunctionFromSrc1(); // Call library functions from 2 different files
  dummyLibFunctionFromSrc2();
#endif

  DDELAY(DBG_LOOP, 2000); // wait for an additional 2 secs  if debugging
  DPRINTSLN(DBG_LOOP, "**loop out **");
}
