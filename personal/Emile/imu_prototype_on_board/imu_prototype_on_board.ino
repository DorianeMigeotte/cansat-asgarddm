#include "Wire.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_LSM9DS0.h"
#include "DebugCSPU.h"

//LSM9DS0 compass; //Copy the folder "HMC5883L" in the folder "C:\Program Files\Arduino\libraries" and restart the arduino IDE.
Adafruit_LSM9DS0 lsm = Adafruit_LSM9DS0();

float magX, magY, magZ;
float accelX, accelY, accelZ;
float gyroX, gyroY, gyroZ;

unsigned long int timeStamp;
unsigned long int startMillis;
unsigned long int millis1;
unsigned long int millis2;

void setup()
{
  Serial.begin(9600);
  Wire.begin();
  if (!lsm.begin())
  {
    /* There was a problem detecting the LSM9DS0 ... check your connections */
    Serial.print(F("Ooops, no LSM9DS0 detected ... Check your wiring!"));
    while (1);
  }
  // 1.) Set the accelerometer range
  lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_4G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_6G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_8G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_16G);

  // 2.) Set the magnetometer sensitivity
  lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_4GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_8GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_12GAUSS);

  // 3.) Setup the gyroscope
  lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_500DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_2000DPS);
  startMillis = millis();
}

void loop()
{
  timeStamp = millis();

  if (millis() - startMillis >= 50) {
    sensors_event_t accel, mag, gyro, temp;
    lsm.getEvent(&accel, &mag, &gyro, &temp);
    millis1 = millis();
    magX = (float)mag.magnetic.x * 100;
    magY = (float)mag.magnetic.y * 100;
    magZ = (float)mag.magnetic.z * 100;

    accelX = (float)accel.acceleration.x;
    accelY = (float)accel.acceleration.y;
    accelZ = (float)accel.acceleration.z;

    gyroX = (float)gyro.gyro.x;
    gyroY = (float)gyro.gyro.y;
    gyroZ = (float)gyro.gyro.z;

    millis2 = millis();

    Serial.print("Magnetometer: X:  ");
    Serial.print(magX);
    Serial.print(" Y: ");
    Serial.print(magY);
    Serial.print(" Z: ");
    Serial.print(magZ);
    Serial.println();

    Serial.print("Accelerometer: X:  ");
    Serial.print(accelX);
    Serial.print(" Y: ");
    Serial.print(accelY);
    Serial.print(" Z: ");
    Serial.print(accelZ);
    Serial.println();

    Serial.print("Gyroscope: X:  ");
    Serial.print(gyroX);
    Serial.print(" Y: ");
    Serial.print(gyroY);
    Serial.print(" Z: ");
    Serial.print(gyroZ);
    Serial.println();
    Serial << "Started at " << millis1 << ", done at " << millis2 << ENDL;
    Serial << "Duration of measurement: " << millis2 - millis1 << ENDL;

    startMillis = timeStamp;
  }
}
