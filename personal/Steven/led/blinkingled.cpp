#pragma once
#include "blinkingled.h"
#include "Arduino.h"

blinkingled::blinkingled(int thePinLed, int theDuration) {
  pinLed = thePinLed;
  duration = theDuration;
  elapsed = 0;
  ts = millis();
}

blinkingled::setup() {
  pinMode(pinLed, OUTPUT);
}

blinkingled::run() {
  elapsed = millis() - ts;
  if (elapsed >= duration) {
    digitalWrite(pinLed, !digitalRead(pinLed));
    ts = millis();
  }
}
