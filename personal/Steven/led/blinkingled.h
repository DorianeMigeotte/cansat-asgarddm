class blinkingled{
  public:
  blinkingled(int thePinLed, int theDuration);
  setup();
  run(); 
  private:
  int pinLed;
  int duration;
  unsigned long ts;
  int elapsed;
};
