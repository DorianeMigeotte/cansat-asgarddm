Serial.println("Memory written");
}

void loop()
{
//
// Read the first page in EEPROM memory, a byte at a time
//
Serial.println("eeprom_read_byte, return: ");

Wire.beginTransmission(DEVADDR);
Wire.write(0x0B);
Wire.endTransmission();
Wire.requestFrom(int(DEVADDR), 1);
if (Wire.available()) {
rdata = Wire.read();
}
//return rdata;
Serial.println(rdata, HEX);

digitalWrite(13, HIGH);
delay(100);
digitalWrite(13, LOW);
delay(100);

}

void eeprom_write_byte(byte deviceaddress, int eeaddress, byte data)
{
// Three lsb of Device address byte are bits 8-10 of eeaddress
byte devaddr = deviceaddress | ((eeaddress >> 8) & 0x07);
byte addr = eeaddress;
Wire.beginTransmission(devaddr);
Wire.write(int(addr));
Wire.write(int(data));
Wire.endTransmission();
delay(10);
}
