#include <XBee.h>

//Pin on Uno (if using one)
const int RF_RxPinOnUno = 9;
const int RF_TxPinOnUno = 11;

//Define RF Serial
# ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
#  else
#  include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#  endif

XBee xbee = XBee();
XBeeResponse response = XBeeResponse();
uint8_t *data;
ZBRxResponse rx = ZBRxResponse();

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(115200);
  RF.begin(115200);
  xbee.setSerial(RF);
  while(!Serial);
  Serial.println("===Beginning of the Tests===");
  Serial.println("Receiver side");
  delay(5000);
}

void loop() {
  xbee.readPacket();
  if (xbee.getResponse().isAvailable()) {
    Serial.println("Packet available");
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
      xbee.getResponse().getZBRxResponse(rx);
      data = rx.getData();
      Serial.println((unsigned)data);
    }
  }
}
