/*
 * EEPROM_Bank.h
 * 
 * This is a subclass of EEPROM_BankWriter completed with methods required for reading.
 */

#pragma once
#include "EEPROM_BankWriter.h"

class EEPROM_Bank : public EEPROM_BankWriter {
public:
  // Overloaded methods
	EEPROM_Bank(const unsigned int theMaintenancePeriodInSec);
	/* Initialize everything according to available hardware.
	 * Assumes hw.getNumExternalEEPROM() > 0. */
	VIRTUAL bool init(const EEPROM_Key theKey, const HardwareScanner &hw, const byte recordSize);

	/* Additionnal operation functions */
	NON_VIRTUAL void resetReader();
	NON_VIRTUAL byte readData(byte* data, const byte dataSize);
	NON_VIRTUAL bool readOneRecord(byte* data, const byte dataSize);
	NON_VIRTUAL bool readerAtEnd();
	NON_VIRTUAL unsigned long leftToRead();
	NON_VIRTUAL unsigned long recordsLeftToRead();
private:
	byte readerCurrentChip;
	unsigned int readerCurrentAddress;
};
