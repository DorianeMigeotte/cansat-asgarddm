void setup() {
  // init the digital pin #13 as OUTPUT
  pinMode(13, OUTPUT);
  Serial.begin(115200);
  while(!Serial) ;
  Serial.println("OK!!!");
}

// the "loop" function is executed again and again (in a infinite loop)
void loop() {
  digitalWrite(13, HIGH);   // Light up the LED (HIGH level = 3.3v)
  delay(1000);              // Wait 1 second
  digitalWrite(13, LOW);    // Switch off the LED (LOW level = 0V)
  delay(1000);              // Wait 1 second
}
