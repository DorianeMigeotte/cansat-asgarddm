/*
 * Just echo on USB_Serial data received from a software serial. 
 */

#define DEBUG
#include "DebugCSPU.h"
#include "SoftwareSerial.h"

SoftwareSerial inputStream(10, 11);  // RX, TX
void setup() {
  DINIT(115200);
  inputStream.begin(9600);
  Serial << "Set up done." <<ENDL;

}

void loop() {
  while (inputStream.available()) {
    char c = inputStream.read();
    Serial << c;
  }
}
