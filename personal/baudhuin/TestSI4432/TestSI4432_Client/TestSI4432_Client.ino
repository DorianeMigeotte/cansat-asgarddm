// rf22_client.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messageing client
// with the RF22 class. RF22 class does not provide for addressing or reliability.
// It is designed to work with the other example rf22_server
//
// Connexions: see TestSI4432_Server
//
#define DEBUG     // To activate debugging functions in DebugCSPU.h
#include "DebugCSPU.h"
#include <RH_RF22.h>

const byte CS_Pin=10;                   // Digital pin used as Chip-Select line for the SS4432
const byte InterruptPin=5;              // Digital pin used by the SS4432 to trigger interruption

// Singleton instance of the radio
RH_RF22 rf22(CS_Pin, InterruptPin);

void setup() 
{
  DINIT(115200);
  Serial << "*** TestSI4432_Client **** " << ENDL;
  delay(1000);
  Serial.println("Set up beginning...");
  Serial.flush();
  
  while (!rf22.init()) {
    Serial.println("RF22 init failed. Retrying....");
    Serial.flush();
    delay(1000);
  }
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
  Serial.println("Setup OK");
}

void loop()
{
  while (1)
  {
    Serial.println("Sending to rf22_server");
    // Send a message to rf22_server
    uint8_t data[] = "Hello World!";
    rf22.send(data, sizeof(data));
   
    rf22.waitPacketSent();
    // Now wait for a reply
    uint8_t buf[RH_RF22_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);

    Serial << "Waiting for reply..." << ENDL; 
    if (rf22.waitAvailableTimeout(500))
    { 
      // Should be a message for us now   
      if (rf22.recv(buf, &len))
      {
        Serial.print("got reply: ");
        Serial.println((char*)buf);
      }
      else
      {
        Serial.println("recv failed");
      }
    }
    else
    {
      Serial.println("No reply, is rf22_server running?");
    }
  }
}
