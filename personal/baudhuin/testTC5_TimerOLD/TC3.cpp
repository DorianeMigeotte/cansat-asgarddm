#include "Arduino.h" 

// This is the code currently in use in the GPS_Client since IsaTwo. Works from 1kHz interrupt but
// I don't really understand why match=4 results in about 1 kHz... 
// This should configure a timer which generates an interruption at a frequency of 8.192/match kHz.
// Tests shows 0.98 kHz for match = 4., 0.68 kHz for match=8. Why ???? 
 void configureTC3(uint16_t match)
{
  uint8_t GCLK_SRC = 4;                                               // clock 4, (0,1,3 are used)
  
  /* NB: macroes are defined in gclk.h and pm.h in folder
         ~/Library/Arduino15/packages/arduino/tools/CMSIS-Atmel/1.2.0/CMSIS/Device/ATMEL/samc21/include/component/
   */
   
  // 1. Configure the generic clock generator division factor with a 32-bits write to the GENDIV register
  //    GENDIV.ID = source Generic Clock Generor
  //  GENDIV.DIV= division factor (prescaler).
  //    There are 9 clock generators numbered 0 to 8. Apparently, 0, 1, 3 are in use.
  //  Division factor is 8 bits for clock generators 0 and 3 to 8, 16 bits for clock generator 1 and 5 for clock generator 2
  //    Clock 4 is used with a division factor of 1. 
  
  GCLK->GENDIV.reg = GCLK_GENDIV_ID(GCLK_SRC) | GCLK_GENDIV_DIV(1);   // source | prescaler
  
  /* 2. Enable the generic clock generator with a 32-bits write to the GENCTRL register
   *    Configuration is for GenericClockGenerator GCLK_SRC ( GCLK_GENCTRL_ID(GCLK_SRC))
   *    which is:   enabled (GCLK_GENCTRL_GENEN)
   *        active during standby mode (GCLK_GENCTRL_RUNSTDBY)
   *        divided by 2^(GENDIV.DIV+1), i.e. 2^2=4. (GCLK_GENCTRL_DIVSEL)
   *        using the XOSC32K oscillator as source (source nr 5, 32.768 kHz) (GCLK_GENCTRL_SRC_XOSC32K)
   *    This results is a clock generator at 32.768/4 kHz = 8.192 kHz.
   */
  //enable clock mod | select the oscilator source | select clock | prescalar | keep running in standby |
  GCLK->GENCTRL.reg = GCLK_GENCTRL_GENEN | GCLK_GENCTRL_SRC_XOSC32K | GCLK_GENCTRL_ID(GCLK_SRC) | GCLK_GENCTRL_RUNSTDBY | GCLK_GENCTRL_DIVSEL;
  while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY);
  
  /* Open the clock gate to the TC3 timer through the clock bus. */
  PM->APBCMASK.reg |= PM_APBCMASK_TC3;                                // Turn the power to the TC3 module on
  
  /*  Configure the generic clock with a 16-bit write to CLKGEN register
   *     Configure the generic clock for timers TCC2 and TC3 (GCLK_CLKCTRL_ID(GCM_TCC2_TC3))
   *     NB: other options are TC4+TC5 and TC6+TC7.
   *     Use the generic clock generator GCLK_SRC (GCLK_CLKCTRL_GEN(GCLK_SRC)
   *     Enable the generic clock GCLK_CLKCTRL_CLKEN
   */
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN |  GCLK_CLKCTRL_GEN(GCLK_SRC) |  GCLK_CLKCTRL_ID(GCM_TCC2_TC3); // tc3 source to clock | generator source | select clock
  while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY);
  
  /* Configure TC3 after disabling it */
  TC3->COUNT16.CTRLA.reg &= ~TC_CTRLA_ENABLE;                         //disable TC3
  while (TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);
  
  /* Count in 16 bits mode, even in standby mode, with a prescaler of 1. */
  TC3->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 | TC_CTRLA_RUNSTDBY | TC_CTRLA_PRESCALER_DIV1 ; // count xbit | standby | prescalar_div1 |
  while (TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY); //wait
  
  /* Configure value which must trigger the interruption in channel 1 */
  TC3->COUNT16.CC[1].reg = match;                                     // Set the compare channel 1 value
  while (TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);//wait
  
  /* Request an interruption when a match is detected with channel 1 */
  TC3->COUNT16.INTENSET.reg = TC_INTENSET_MC1;                        // enable interrupt
  while (TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);//wait
  
  TC3->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;                          // enable TC3
  while (TC3->COUNT16.STATUS.reg & TC_STATUS_SYNCBUSY);//wait
  
  NVIC_EnableIRQ(TC3_IRQn);                                           // Enable the TC3 interrupt vector
  NVIC_SetPriority(TC3_IRQn, 0x00);                                   // Set the priority to max
}
