 class  OO_Sos {

  public:
    OO_Sos(byte pinNumber);
    void run();
    
  private:
    bool OO_Sos::runSign(unsigned int duration) ;
    bool OO_Sos::runCharacter() ;
    byte currentChar; // numéro du caractère en cours d'émission. 0=S, 1=O, 2=S 
    byte currentSign; // numéro du signe+blanc en cours d'émission. 0=pas commencé. 
    byte currentSignStep;  // 0=pas commencé, 1 = allumé, 2=éteint pour espace
    elapsedMillis elapsed; // l'instant du dernier changement.
 }
