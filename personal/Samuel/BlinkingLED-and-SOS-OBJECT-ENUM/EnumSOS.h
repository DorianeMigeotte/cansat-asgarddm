#pragma once
#include "Arduino.h"

class EnumSOS{                                                            //Creates the class EnumSOS

  public :                                                                   //Acces specifier for public members  
    EnumSOS(byte thePinNumber, uint16_t thePauseTime);                    //Declares the constructor EnumSOS with two argument that are the pin number the SOS will be ran on and the time that wille be between the sos

    void run();                                                              //declares the run() method
    
  private :                                                                  //Acces specifier for public members 

  enum class SosState {                                                      //Declares the enumerated class SosState      
        BipS,                                                                //Declares the state for the first state of the switch
        BipO,                                                                //Declares the state for the second state of the switch
        SndBipS,
        PauseState                                                           //Declares the state for the third state of the switch
        };
  void ThreeBeeps (uint16_t sosInterval,SosState nextState);
  byte pinNumber;                                                            //Declares a variable that will store the pin number
  unsigned long ts;                                                          //Declares a variable for a time stamp
  SosState state;                                                            //Declares the variable that will store the state of the switch
  byte counter;                                                              //Declares a counter
  uint16_t pauseTime;                                                        //Declares a variable that will store a user inputted time that will elapse between the sos'es
};
