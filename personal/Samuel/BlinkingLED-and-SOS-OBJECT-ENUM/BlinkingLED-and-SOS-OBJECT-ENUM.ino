#include "BlinkingLED.h"

#include "EnumSOS.h"

BlinkingLED led1(10,50);             //Declares a BlinkingLED type object called led1
EnumSOS eSos1 (11,2000);

void setup() {
Serial.begin(9600);                 //Starts the serial cast at a 9600 rate
}

void loop() { 
  led1.run();                       //calls the method run() of led1
  eSos1.run();                      //calls the method run() of eSos1
}
