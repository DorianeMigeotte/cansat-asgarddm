#include <Wire.h>
#define DEBUG
#include "DebugCSPU.h"
#include "IsaTwoConfig.h"

#include "ClockSynchronizer.h"

#define I2C_ADDRESS_ME 0x8

constexpr byte CtrlLine = 10;
constexpr unsigned int DelayInMainLoop=1; // msec. 

enum class ImagerState_t {
  Inactive,
  WaitingForClock,
  Imaging
};

ImagerState_t state = ImagerState_t::Inactive;

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  Serial << "============================================" << ENDL;
  Serial << "=    Master/Imager synchro demonstrator    =" << ENDL;
  Serial << "=              (Imager side)               =" << ENDL;
  Serial << "============================================" << ENDL << ENDL;

  Serial.print("CtrlLine =");
  Serial.println(CtrlLine);
  pinMode(CtrlLine, INPUT);
  Serial.println("Waiting for ctrl line to go LOW...");

  /* At startup we are acting as slave */

}

void configureToReceiveClockFromMaster() {
  Wire.begin(I2C_SlaveControllerAddress);
  ClockSynchronizer_Slave::begin(); 
}

bool ConfigureForImaging() {
  Wire.begin();
  Serial.println("turn on regulator, wait, init...");
  bool initializationResult=true;
  delay(2000);
  // if error, keep configuring, else image. 
  return (initializationResult);
}

void shutdownImager() {
  Serial.println("turn off regulator...");
}

void loop() {
   unsigned long masterClock;
  //if (digitalRead(CtrlLine) == LOW) Serial.println("LOW");
  //else Serial.println("HIGH");

  if (digitalRead(CtrlLine) == LOW)
  {
    switch (state)
    {
      case ImagerState_t::Inactive:
        Serial.println("Waiting for clock from master");
        state = ImagerState_t::WaitingForClock; // This must be set before actually switching.
        configureToReceiveClockFromMaster();
        break;
      case ImagerState_t::WaitingForClock:
        if (ClockSynchronizer_Slave::isSynchronized()) {
          Serial.println("Synchronized!");
          if (ConfigureForImaging()) {
            state=ImagerState_t::Imaging;
          }
        }
        break;
      case ImagerState_t::Imaging:
        if (ClockSynchronizer_Slave::getMasterClock(masterClock)) {
        Serial.print("Imaging.... MasterClock");
        Serial.println(masterClock);
        delay(1000);}
        else {
          Serial.println("Error requesting masterClock");
        }
        break;
      default:
        Serial.println("Error: unexpected state");
    } // switch
  }
  else
  {
    if (state != ImagerState_t::Inactive) {
       shutdownImager();
       state=ImagerState_t::Inactive;
        Serial.println("Line HIGH going inactive...");
    }
  }

  delay(DelayInMainLoop); // Keep short to avoid blocking the master
}
