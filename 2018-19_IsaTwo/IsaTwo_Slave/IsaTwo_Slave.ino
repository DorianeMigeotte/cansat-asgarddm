/*
   IsaTwo_Slave

   The software running on the slave µController (the "imager") of the IsaTwo project.
*/

#define DEBUG
#include "DebugCSPU.h"
#include <Wire.h>
#include <SPI.h>
#include "IsaTwoImager.h"
#include "IsaTwoConfig.h"
#include "elapsedMillis.h"

#define DBG_IMAGER_FSM 1
#define DBG_REPORT_IMAGES 1
#define DBG_FSM_EVERY_LOOP 0
#define DBG_doImage 0

constexpr unsigned long HeartBeatPeriod_Waiting = 1000; // msec.
constexpr unsigned long HeartBeatPeriod_Error = 300; // msec.
constexpr unsigned long HeartBeatPeriod_Imaging = 3000; // msec.

// State for the imager state machine.
enum class ImagerState_t {
  Inactive,
  Imaging
};

unsigned long heartBeatPeriod = HeartBeatPeriod_Waiting; // msec.

IsaTwoImager img(ImagerCamera_ChipSelect);
bool regulatorOn = false;
ImagerState_t state;
elapsedMillis heartBeatDelay;
unsigned long activationTimestamp;  // The moment the imager was activated by the master.

void powerDown() {
  if (regulatorOn)
  {
    digitalWrite(ImagerRegulatorEnablePin, LOW); 
    regulatorOn = false;
    DPRINTSLN(DBG_IMAGER_FSM, "Regulator now OFF");
  }
  else {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: regulator already off");
  }
}

void powerUp() {
  if (!regulatorOn) {
    digitalWrite(ImagerRegulatorEnablePin, HIGH);
    regulatorOn = true;
    DPRINTSLN(DBG_IMAGER_FSM, "Regulator now ON");
  }
  else {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: regulator already on");
  }
}

void blinkToNotifyError() {
  elapsedMillis ts;
  while (ts < 10000) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    delay(HeartBeatPeriod_Error);
  }
}

bool activateImager() {
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(ImagingLED_DigitalPinNbr, HIGH);
  powerUp();
  DPRINTS(DBG_DIAGNOSTIC, "Waiting after powerup (");
  DPRINT(DBG_DIAGNOSTIC, Imager_DelayAfterRegulatorOn);
  DPRINTSLN(DBG_DIAGNOSTIC, " msec)");
  delay(Imager_DelayAfterRegulatorOn);

  DPRINTSLN(DBG_DIAGNOSTIC, "Initialising the IsaTwoImager...");
  bool imagerOK = false;
  // Retry as long as ctrl line is down.
  digitalWrite(ImagingLED_DigitalPinNbr, LOW);
  digitalWrite(LED_BUILTIN, LOW);
  while (!imagerOK && (digitalRead(ImagerCtrl_SlaveDigitalPinNbr) == LOW))
  {
    imagerOK = img.begin(ImagerSD_CardChipSelect, ImagerCamera_SPI_Speed);
    if (!imagerOK) {
      DPRINTSLN(DBG_DIAGNOSTIC, "Error during IsaTwoImager.begin()");
      blinkToNotifyError();
    }
    else {
      // Blink both LEDS: init OK
      for (int i = 0; i < 5; i++) {
        digitalWrite(LED_BUILTIN, HIGH);
        digitalWrite(ImagingLED_DigitalPinNbr, HIGH);
        delay(60);
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(ImagingLED_DigitalPinNbr, LOW);
        delay(60);
      } // for
    } // else
  } // while
  return (imagerOK);
}

void doImage() {
  static bool largeImage = true;
  DPRINTS(DBG_doImage, "DoImage begin");
  if (largeImage) {
    img.setImageSize(Imager_LargeImageSize);
    DPRINTS(DBG_DIAGNOSTIC, "L: ");
  } else {
    img.setImageSize(Imager_SmallImageSize);
    DPRINTS(DBG_DIAGNOSTIC, "S: ");
  }

  unsigned long actualTime = millis();
  unsigned long timeSinceActivation = actualTime - activationTimestamp;
  digitalWrite(ImagingLED_DigitalPinNbr, HIGH);
  bool result = img.image(timeSinceActivation);
  digitalWrite(ImagingLED_DigitalPinNbr, LOW);
  if (!result)
  {
    DPRINTS(DBG_DIAGNOSTIC, " *** Error while imaging at ");
    DPRINTLN(DBG_DIAGNOSTIC, timeSinceActivation);
  }
  else {
    DPRINTS(DBG_REPORT_IMAGES, "Imaging ok, started at ");
    DPRINT(DBG_REPORT_IMAGES, actualTime);
    DPRINTS(DBG_REPORT_IMAGES, ", image timestamp=");
    DPRINTLN(DBG_REPORT_IMAGES, timeSinceActivation);
    largeImage = !largeImage;
  }
  delay(Imager_DelayAfterEachImage);
}

#ifdef INIT_SERIAL
void printWelcomeBoard() {
  Serial << "============================================" << ENDL;
  Serial << "=    Imager (slave) Controller software    =" << ENDL;
  Serial << "============================================" << ENDL << ENDL;
  Serial <<  "###" << ENDL;
  Serial <<  " #   #    #    ##     ####   ######  #####" << ENDL;
  Serial <<  " #   ##  ##   #  #   #    #  #       #    #" << ENDL;
  Serial <<  " #   # ## #  #    #  #       #####   #    #" << ENDL;
  Serial <<  " #   #    #  ######  #  ###  #       #####" << ENDL;
  Serial <<  " #   #    #  #    #  #    #  #       #   #" << ENDL;
  Serial <<  "###  #    #  #    #   ####   ######  #    #" << ENDL << ENDL;
#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "*** Warning: Not running on Feather M0 Express board ??? ***" << ENDL << ENDL;
#endif

  Serial << "Hardware pins:" << ENDL;
  Serial << "  Ctrl line (active LOW)        : " << ImagerCtrl_SlaveDigitalPinNbr << ENDL;
  Serial << "  Serial debug (active LOW)     : " << DbgCtrl_ImagerDigitalPinNbr
         << " (" << digitalRead(DbgCtrl_ImagerDigitalPinNbr) << ")" << ENDL;
  Serial << "  Regulator enable (active HIGH): " << ImagerRegulatorEnablePin << ENDL;
  Serial << "  SD Card chip-select           : " <<  ImagerSD_CardChipSelect <<  ENDL;
  Serial << "  Camera chip-select            : " <<  ImagerCamera_ChipSelect <<  ENDL;
  Serial << "  Imaging LED                   : " <<  ImagingLED_DigitalPinNbr <<  ENDL;
  Serial << "SPI frequency for camera        : " << ImagerCamera_SPI_Speed / 1000000 << "MHz" << ENDL;
  Serial << "Delay after regulator on        : " << Imager_DelayAfterRegulatorOn << " msec" << ENDL;
  Serial << "Delay after each image          : " << Imager_DelayAfterEachImage << " msec" << ENDL;
}
#endif

void  setup() {
#ifdef INIT_SERIAL
  DINIT_IF_PIN(USB_SerialBaudRate, DbgCtrl_ImagerDigitalPinNbr);
  printWelcomeBoard();
#endif

  pinMode (ImagerCtrl_SlaveDigitalPinNbr, INPUT_PULLUP);

  pinMode (ImagerRegulatorEnablePin, OUTPUT);
  digitalWrite (ImagerRegulatorEnablePin, LOW);
  regulatorOn = false;

  pinMode (ImagingLED_DigitalPinNbr, OUTPUT);
  digitalWrite(ImagingLED_DigitalPinNbr, LOW);

  pinMode (LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  SPI.begin();
  Wire.begin();
  heartBeatDelay = 0;

  state = ImagerState_t::Inactive;
  DPRINTSLN(DBG_IMAGER_FSM, "Inactive. Waiting for ctrl line to go LOW...");
  DPRINTSLN(DBG_DIAGNOSTIC, "Init is OK");
}


void loop() {
  if (digitalRead(ImagerCtrl_SlaveDigitalPinNbr) == LOW)
  {
    switch (state)
    {
      case ImagerState_t::Inactive:
        activationTimestamp = millis(); // take this time immediately!
        DPRINTSLN(DBG_IMAGER_FSM, "\n  Activation received while inactive");
        if (activateImager()) {
          state = ImagerState_t::Imaging;
          heartBeatPeriod = HeartBeatPeriod_Imaging;
          DPRINTS(DBG_IMAGER_FSM, "  Switching to 'Imaging' at ");
          DPRINTLN(DBG_IMAGER_FSM, activationTimestamp);
        }
        else {
          powerDown();
          state = ImagerState_t::Inactive;
          DPRINTS(DBG_IMAGER_FSM, "Activation failed: return to inactive...");
        }
        break;
      case ImagerState_t::Imaging:
        DPRINTSLN(DBG_FSM_EVERY_LOOP, "Imaging");
        doImage();
        break;
      default:
        DPRINTS(DBG_DIAGNOSTIC, "Error: unexpected state in Imager FSM");
    } // switch
  }
  else
  {
    heartBeatPeriod = HeartBeatPeriod_Waiting;
    if (regulatorOn)
    {
      powerDown();
    }
    if (state != ImagerState_t::Inactive) {
      state = ImagerState_t::Inactive;
      DPRINTS(DBG_IMAGER_FSM, "Ctrl line HIGH: going inactive, Regulator is off.");
    }
  } // else

  if (heartBeatDelay > heartBeatPeriod) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    heartBeatDelay = 0;
  }

  if (Imager_LoopDelay) delay(Imager_LoopDelay);
}
