/*
 * HardwareScanner
 * 
 * This utility just creates a HardwareScanner and prints diagnostic on the Serial output.
 * Only information that can be automatically discovered is shown. 
 * 
 */

#include "HardwareScanner.h" // Include this first to have libraries linked in the right order. 
 
 void setup() {
  Serial.begin(115200);
  while (!Serial) ;

  Serial.println("Running Hardware Scanner...");
  Serial.println("(Remember the I2C bus scanning will take a *very* long time on some boards");
  Serial.println(" if pull-ups are not installed on the SCL and SDA lines)");
  Serial.flush();
  
  HardwareScanner hw;
  Serial.println("Collecting information...");
  hw.init(1,127);
  Serial.println("Full diagnostic:");
  hw.printFullDiagnostic(Serial);
}

void loop() {
  // put your main code here, to run repeatedly:

}
