/*
    test_ADC_UsageFeatherUno.ino

    Test for the analog input readings
*/

/*
 * USAGE: CONNECT ALL ANALOG PINS TO GROUND AND CONNECT THE TESTED PINS TO 3.3V OR 5V DEPENDING ON THE USED BOARD 
 */
#include <DebugCSPU.h>
#define DEBUG

float voltageReference = 0;
int ADC_Steps = 0;

void setup() {
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
   analogReadResolution(12);  // Configure 12 bits ADC (defaults to 10 bits).
   voltageReference = 3.3;
   ADC_Steps = 4095.0;
   analogReference(AR_DEFAULT);
#else
  voltageReference = 5.0;
  ADC_Steps = 1023;
  analogReference(DEFAULT);
#endif
  Serial.begin(115200);
  Serial.print("** TEST INITIATED **");
}

void loop() {
  Serial << F("---------------------------------------------------------") << ENDL;
  for (int i = 14; i < 20; i++) {
    int rawValue = analogRead(i);
    float Voltage = rawValue * (voltageReference / ADC_Steps);
    Serial << F("Pin ") << i << F(":");
    Serial.print(rawValue,4); 
    Serial << F(" ==> ") << Voltage << F("V") << ENDL;
  }
  delay(1000);
}
