/*
    echoSerial2OnUSB

    A small utility which dumps serial data received on Serial2 RX (or pin RX_DATA on Uno) to the USB Serial, 
    and sends serial data received on USB Serial on Serial2 TX (or pin TX_DATA on Uno). 

    On Feather Serial2: TX = D10, RX = D11

*/

constexpr byte RX_DATA = 10; // Serial RX in case a Uno board is used.
constexpr byte TX_DATA = 11; // Serial TX in case a Uno board is used.

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#include "Serial2.h"
#else
#include "SoftwareSerial.h"
SoftwareSerial Serial2(RX_DATA, TX_DATA);
#endif

#define DEBUG
#include "DebugCSPU.h"

void setup() {
  DINIT(115200);
  Serial2.begin(115200);

  Serial << "Init ok." << ENDL;
#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "Echoing with SoftwareSerial, from pin #" << RX_DATA << " to USB Serial" << ENDL;
  Serial << "Echoing with SoftwareSerial, from USB Serial to pin #" << TX_DATA << " to USB Serial" << ENDL;
#else
  Serial << "Echoing from hardware Serial 2 (TX=D10, RX=D11) to USB Serial and conversely" << ENDL;
#endif
}

void loop() {
  while (Serial2.available()) {
    Serial << (char) Serial2.read();
  }
  while (Serial.available()) {
    Serial << ".";
    Serial2 << (char) Serial.read();
  }
}
