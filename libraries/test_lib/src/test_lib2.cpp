/*
   Prior to including this file, the sketch must include "cansat_debug.h" to have the
   macro definitions. A library cannot possibly include another library's header file
   in the Arduino IDE.
*/

//------------- Debugging control: never place in header file! -------
//#define DEBUG              // define to enable debugging for this library
#include "DebugCSPU.h"  
#include "Arduino.h" 

#define DBG_TEST_LIB2 1
//--------------------------------------------------------------------

#include "test_lib2.h"

int dummyLibFunctionFromSrc2 () {
  DPRINTSLN(DBG_TEST_LIB2, "Library called (src2): blinking 4 times...");
  for (int i = 0 ; i < 4; i++) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(durationOn);
    digitalWrite(LED_BUILTIN, LOW);
    delay(durationOn);
  }
  delay(500);
  return 0;
}

