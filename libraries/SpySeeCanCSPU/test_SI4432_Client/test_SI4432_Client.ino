#include "SI4432_Client.h"

SI4432_Client si4432(5, 10);
powerDBm_t readings[10];
powerDBm_t readings2[11];
void setup() {
  Serial.begin(115200);
  while(!Serial);
  si4432.begin();
}

void loop() {
  Serial.println("1 read : ");
  Serial.flush();
  Serial.println(si4432.readPower(500));
  Serial.println(si4432.readPower(450));
  Serial.println(si4432.readPower(550));
  si4432.readPower(readings, 10, 433, 866,30);
  si4432.readPower(readings2, 11, 433, 533, 10);
  Serial.println("10 reads");
  for(unsigned int i = 0; i< 10; i++){
    Serial.print("First table");
    Serial.print("Channel ");
    Serial.print(i);
    Serial.print(" : ");
    Serial.print(readings[i]);
    Serial.println("  (dbm) ");
  }
  Serial.println();
  for(unsigned int i = 0; i< 11; i++){
    Serial.print("Second table");
    Serial.print("Channel ");
    Serial.print(i);
    Serial.print(" : ");
    Serial.print(readings2[i]);
    Serial.println("  (dbm) ");
  }
  Serial.println();Serial.println();
  delay(500);
}
