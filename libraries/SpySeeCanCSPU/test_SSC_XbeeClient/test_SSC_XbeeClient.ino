/*
    Test program for the use of the XBee API mode through streaming operators,
    + binary transfer of SSC_Record.
*/

#define RF_ACTIVATE_API_MODE  // If undefined, transparent mode is used. 
#include "SSC_XBeeClient.h"
#include "elapsedMillis.h"

// TO FIX AFTER TEST IS SUCCESSFULL
//#include "IsaTwoHW_Scanner.h" // For OPEN_RF_STRING/CLOSE_RF_STRING

constexpr uint8_t StringBufferSize = 150;
constexpr unsigned int MaxNumberOfErrors = 10; // When this number of error is reached, the test stops.
constexpr bool DisplayOutgoingFrame = true;
constexpr bool DisplayIncomingFrame = true;
constexpr bool DocumentEmission = true;
constexpr int ackTimeOut = 500; // Time allowed to get ack from destination card.

#define RF-TSCV   // Define or undefine according to the XBee module connected.
#undef A //Define for pair A or undefine for pair B

#ifdef RF-TSCV
constexpr unsigned long EmissionPeriod = 1000; // msec. The emission period
# ifdef A
SSC_XBeeClient xb(0x0013a200, 0x418fb90a); // Address of Can_A Xbee to load in RF-TSCV_A one.
# else //Pair B
SSC_XBeeClient xb(0x0013a200, 0x418fbbeb); // Address of Can_B Xbee to load in RF-TSCV_B one.
# endif
#else
constexpr unsigned long EmissionPeriod = 70; // msec. The emission period
# ifdef A
SSC_XBeeClient xb(0x0013a200, 0x41827f67); // Address of RF-TSCV_A Xbee to load in the Can_A one.
# else //Pair B
SSC_XBeeClient xb(0x0013a200, 0x418fc78b); // Address of RF-TSCV_B Xbee to load in the Can_B one.
# endif
#endif
SSC_XBeeClient* xbPtr = &xb;

elapsedMillis elapsed;
unsigned int numErrors = 0;

void setup() {
  DINIT(115200);
  Serial1.begin(115200);
  xb.begin(Serial1);
  Serial << "Set up OK" << ENDL;
}

void sendRecord() {

  if (DocumentEmission) {
    Serial << ENDL <<  "C. Sending an SSC_Record" << ENDL;
  }
  
  // Send a record
  SSC_Record rec;
  rec.newGPS_Measures = true;
#ifdef INCLUDE_GPS_VELOCITY
  rec.GPS_VelocityKnots = 36;
  rec.GPS_VelocityAngleDegrees = 27;
#endif
  rec.temperatureBMP = 15;
  rec.altitude = 97;
  rec.temperatureThermistor = 20;
  rec.timestamp = 12345;
  rec.GPS_LongitudeDegrees = 98.765;
  rec.GPS_LatitudeDegrees = 34.56789;
  rec.pressure = 1013.7;

  if (DisplayOutgoingFrame) {
    Serial << "Record content: " << ENDL;
    rec.print(Serial);
  }
  // fill record here
  xb.send(rec, ackTimeOut);

  if (!DocumentEmission) {
    Serial << '.' ;
  }
}


bool checkRecord(const SSC_Record &rec) {
  bool ok = false;
  if (  (rec.newGPS_Measures          != true) ||
        (rec.temperatureBMP           != 15) ||
        (rec.altitude                 != 97) ||
        (rec.temperatureThermistor    != 20) ||
#ifdef INCLUDE_GPS_VELOCITY
        (rec.GPS_VelocityKnots        != 36) ||
        (rec.GPS_VelocityAngleDegrees != 27) ||
#endif
        (rec.timestamp                != 12345)) {

    Serial << "*** Error in extracted record (int)." << ENDL;
    rec.print(Serial);
    numErrors++;
  } else if (( fabs(rec.GPS_LongitudeDegrees - 98.765) > 0.001) ||
             (fabs(rec.GPS_LatitudeDegrees  - 34.56789) > 0.001) ||
             (fabs(rec.pressure - 1013.7) > 0.001)
            )
  {
    Serial << "*** Error in extracted record (float)." << ENDL;
    rec.print(Serial);
    numErrors++;
  } else {
    ok = true;
  }

  return ok;
}

void loop() {
  static int okCounter = 0;
  if (numErrors >= MaxNumberOfErrors) {
    delay(100);
    return;
  }

  if (elapsed >= EmissionPeriod) {
    sendRecord();
    elapsed = 0;
  }

  SSC_Record rec;
  if (xb.receiveRecord(rec)) {
    Serial << " Record received< " << ENDL;
    okCounter++;
    okCounter = okCounter % 10;
    if (okCounter == 0) Serial << ENDL;

    checkRecord(rec);
  }

  if (numErrors >= MaxNumberOfErrors) {
    Serial << "*** Max number of errors reached ("
           << MaxNumberOfErrors << "). Test over. " << ENDL;
  }

}
