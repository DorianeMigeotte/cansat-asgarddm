/*
 * SSC_RecordGoodies
 */

 /**
 * @brief This file gathers the implementation of all SSC_Record methods which are not used in an
 * operational context. This avoid those methods to be linked in the operational software, hence 
 * saving memory.
 */

#include "SSC_Record.h"
#define DEBUG
#include <DebugCSPU.h>
#define DBG 1

void SSC_Record::print(Stream& str, const float arr[3]) const {
  str << '[';
  str.print(arr[0], numDecimalPositionsToUse); str << ',';
  str.print(arr[1], numDecimalPositionsToUse); str << ',';
  str.print(arr[2], numDecimalPositionsToUse);
  str << ']';
}

void SSC_Record::print(Stream& str, const int16_t arr[3]) const {
  str << '[' << arr[0] << ',' << arr[1] << ',' << arr[2] << ']';
}

void SSC_Record::print_Part(Stream& str, DataSelector select) const {
  switch (select) {
    case DataSelector::GPS :
      str << ENDL;
      str << (F("** GPS DATA **")) << ENDL;
      str << (F(" GPS_Measures           : ")); printCSV(str, newGPS_Measures); str << ENDL;
      str << (F(" GPS_Latitude (deg)     : ")); printCSV(str, GPS_LatitudeDegrees); str << ENDL;
      str << (F(" GPS_Longitude(deg)     : ")); printCSV(str, GPS_LongitudeDegrees); str << ENDL;
      str << (F(" GPS_Altitude (m)       : ")); printCSV(str, GPS_Altitude); str << ENDL;
#ifdef INCLUDE_GPS_VELOCITY
      str << (F(" GPS_Velocity (knots)   : ")); printCSV(str, GPS_VelocityKnots); str << ENDL;
      str << (F(" GPS_VelocityAngle (deg): ")); printCSV(str, GPS_VelocityAngleDegrees); str << ENDL;
#endif

      break;
    case DataSelector::PrimarySecondary :
      str << (F("** Primary/secundary Mission **")) << ENDL;
      str << (F(" Temp.BMP (°C) : ")); printCSV(str, temperatureBMP); str << ENDL;
      str << (F(" Pressure (hPa): ")); printCSV(str, pressure); str << ENDL;
      str << (F(" Altitude (m)  : ")); printCSV(str, altitude); str << ENDL;
      str << (F(" Temp.Thermitor1 (°C): ")); printCSV(str, temperatureThermistor1); str << ENDL;
	  str << (F(" Temp.Thermitor2 (°C): ")); printCSV(str, temperatureThermistor2); str << ENDL;
	  str << (F(" Temp.Thermitor3 (°C): ")); printCSV(str, temperatureThermistor3); str << ENDL;
      str << (F(" TBC           : ")); printCSV(str, 0.0f); str << ENDL;

      break;
    default:
      DPRINT(DBG,"Unexpected DataSelector value. Terminating Program");
      DASSERT(false);
  }
}
void SSC_Record::print(Stream& str, DataSelector select) const {
  str << F("Record type = DataRecord, ts (msec) = ") << timestamp << ENDL;
  if (select == DataSelector::All) {
    print_Part(str, DataSelector::GPS);
    print_Part(str, DataSelector::PrimarySecondary);
  } else {
    print_Part(str, select);
  }
}
