/*
 * SI4432_Client.cpp
 */
 
#include "SI4432_Client.h"
#define DEBUG
#include "DebugCSPU.h"
#define DBG_BEGIN 1
#define DBG_FREQUENCY_SUPPORTED 1
#define DBG_FREQUENCY_CHANGED 1
#define DBG_CONVERSION 1
#define DBG_DIAGNOSTIC 1

SI4432_Client::SI4432_Client(uint8_t theSlaveSelectPin, uint8_t theInterruptPin) : rf22(theSlaveSelectPin, theInterruptPin){}

bool SI4432_Client::begin(float minFreq, float maxFreq, float stepFreq){
  DINIT(115200);
  DPRINTSLN(DBG_BEGIN, "Begin");
  if(minFreq == 0.0) minFreq = minSupportedFrequency;
  if(maxFreq == 0.0) maxFreq = maxSupportedFrequency;
  if(stepFreq == 0.0) stepFreq = defaultStepFrequency;
  if(minFreq < minSupportedFrequency || maxFreq > maxSupportedFrequency){
    DPRINTSLN(DBG_FREQUENCY_SUPPORTED, "Frequencies provided not supported");
    return false;
  }
  minFrequency = minFreq;
  maxFrequency = maxFreq;
  stepFrequency = stepFreq;
	if (!rf22.init()){
    DPRINTSLN(DBG_BEGIN, "RF22 init failed");
		return false;
	}// Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36

	rf22.setModeRx();
	DPRINTS(DBG_BEGIN, "Mode changed for : ");
	DPRINTLN(DBG_BEGIN, rf22.mode());
	rf22.setFrequency(minFrequency);
	if(!rf22.setFrequency(minFrequency)){
	  DPRINTSLN(DBG_BEGIN||DBG_FREQUENCY_CHANGED, "*** Frequency not changed! ***");
		return false;
	}
	return true;
}

powerDBm_t SI4432_Client::dBmConversion(uint8_t rssi){
  DPRINTS(DBG_CONVERSION, "RSSI = ");
  DPRINTLN(DBG_CONVERSION, rssi);
  powerDBm_t result = 255;
	if(rssi < 218){
	  result = 0.537671131*rssi - 124.5550595238;
	}
	else if(rssi >= 218 && rssi < 228){
	  result = 0.9417006803*rssi - 226.6380119916;
	}
 DPRINTS(DBG_CONVERSION, "dBm = ");
  DPRINTLN(DBG_CONVERSION, result);
  return result;
}

powerDBm_t SI4432_Client::readPower(float frequencyMHz){ 
	rf22.setFrequency(frequencyMHz);
  if(!rf22.setFrequency(frequencyMHz)){
    DPRINTSLN(DBG_FREQUENCY_CHANGED, "*** Frequency not changed! ***");
  }
	delay(2);
  int16_t power = 0;
  for (unsigned int i = 0; i < 10; i++){
    power += rf22.rssiRead();
  }
  power /= 10;
	return dBmConversion(power);
}

bool SI4432_Client::readPower(powerDBm_t power[], byte nbData, float minFreq, float maxFreq, float stepFreq){
  if(minFreq == 0.0) minFreq = minFrequency;
  if(maxFreq == 0.0) maxFreq = maxFrequency;
  if(stepFreq == 0.0) stepFreq = stepFrequency;
  
	if(((maxFreq-minFreq)/stepFreq) + 1 > nbData){
    DPRINTSLN(DBG_DIAGNOSTIC, "Invalid parameters provided to scan !!!");
		return false;
	}
 if(minFreq < minSupportedFrequency || maxFreq > maxSupportedFrequency){
    DPRINTSLN(DBG_FREQUENCY_SUPPORTED, "Frequencies provided not supported");
    return false;
  }
  
	float f = minFreq;
  unsigned int i = 0;
	while(f <= maxFreq && i < nbData){
	  power[i] = readPower(f);
	  f += stepFreq;
    i++;
	}
  return true;
}

bool SI4432_Client::readData(SSC_Record& record){
  return false;

}
