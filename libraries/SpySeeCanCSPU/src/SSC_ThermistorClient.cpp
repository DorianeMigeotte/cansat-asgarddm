/*
   SSC_ThermistorClient.cpp
*/

#include "SSC_ThermistorClient.h"
/** @ingroup SpySeeCanCSPU
    @brief (class to use the thermistors?)
*/

SSC_ThermistorClient::SSC_ThermistorClient():
  thermistor1(A0, 10000.0),thermistor2(A1, 10000.0), thermistor3(A2, 10000.0)
{}

bool SSC_ThermistorClient::readData(SSC_Record& record) {
  record.temperatureThermistor1 = thermistor1.readTemperature();
  record.temperatureThermistor2 = thermistor2.readTemperature();
  record.temperatureThermistor3 = thermistor3.readTemperature();
  return true;
}
