/*
    This program tests the ThermistorClient class with the following 3 thermistors:
     - NTCLE100E3
     - NTCLG100E2
     - VMA320 (break-out)

    Wiring:
      VCC - NTCLE100E3 - A0 -  10kOhm - GND
      VCC - NTCLG100E2 - A1 - 10kOhm - GND
      VMA320, looking from components side, thermistor facing up:
      - A2 to left
      - VCC to middle
      - Gnd to right.

     Usage: use csvFormat constant to select between human-readable output and csv output (for use in Excel sheet);
*/

constexpr bool csvFormat = true;// SEt to true to generate CSV formatted output.

// (Constants discribing wiring and thermistor models are now in there classes).

#define DEBUG
#include "DebugCSPU.h"
#include "ThermistorClient.h"
#include "VMA320_Client.h"
#include "NTCLE100E3_Client.h"
#include "NTCLG100E2_Client.h"

NTCLE100E3_Client NTCLE100E3(A0, 10000.0);
NTCLG100E2_Client NTCLG100E2(A1, 10000.0);
VMA320_Client VMA320(A2, 10000.0);
ThermistorClient VMA320_Simple(A2, 10000.0, 1 / 298.15, 1 / 3950.0, 0, 0, 10000);

void setup() {
  // to know wich Vmax and Nsteps we have to use;

  DINIT(115200);
  if (csvFormat) {
    Serial << "time, NTCLE100E3, NTCLG100E2, VMA320" << ENDL;
  }
}

void loop() {
  // put your main code here, to run repeatedly:

  float temp_A = NTCLE100E3.readTemperature();
  float temp_B = NTCLG100E2.readTemperature();
  float temp_C = VMA320.readTemperature();
  float temp_C_simple = VMA320_Simple.readTemperature();

  if (csvFormat) {
    Serial <<  millis() << ", " << temp_A  << ", " << temp_B << ", " << temp_C << ", " << temp_C_simple <<  ENDL;

  } else {
    Serial << "Reading first thermistor..." << ENDL;
    Serial << "Reading second thermistor..." << ENDL;
    Serial << "Reading third thermistor..." << ENDL;
    Serial <<  millis() << ", NTCLE100E3: " << temp_A  << "°C, NTCLG100E2: " << temp_B  << "°C, VMA320: " << temp_C << "°C (simplifié: " << temp_C_simple << "°C)" <<  ENDL;
  }
  delay(1000);
}
