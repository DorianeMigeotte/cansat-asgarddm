/*
 * Serial2.cpp
 */

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS

#include "Serial2.h"
#include "wiring_private.h"

SercomSerial1 Serial2;

SercomSerial1::SercomSerial1()
  : Uart(&sercom1,  11, 10 ,  SERCOM_RX_PAD_0 , UART_TX_PAD_2), rx(11), tx(10) {};

void SercomSerial1::begin(unsigned long baudRate) 
{
  Uart::begin(baudRate);
  pinPeripheral(rx, PIO_SERCOM); // Pins 10/11 must be configured to use the Sercom Mux
  pinPeripheral(tx, PIO_SERCOM); // Warning: this cannot be done in the constructor!
}

// Interrupt request handler
void SERCOM1_Handler()
{
  Serial2.IrqHandler();
}
#endif
