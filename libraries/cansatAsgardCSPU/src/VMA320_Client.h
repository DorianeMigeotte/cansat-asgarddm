/*
   VMA320_Client.h
*/

#pragma  once
#include "ThermistorClient.h"

/**
  @brief This a subclass of thermistorClient customized for thermistor VMA320.
  Use this class to read thermistor resistance and convert to degrees.
  wiring VCC to thermistor, thermistor to serialresistor, serialresistor to ground.
*/
class VMA320_Client: public ThermistorClient {
  public:
    /**
      @param theAnalogPinNbr The pin connected to the thermistor-to-resistor connexion.
      @param theSerialResistor The value of the resistor connected to the thermistor (ohms).
    */
    VMA320_Client(byte theAnalogPinNbr, float theSerialResistor ):
      ThermistorClient(theAnalogPinNbr, 1.0, 1.009249522e-03,  2.378405444e-04, 0.0, 2.019202697e-07, theSerialResistor) {};
};
