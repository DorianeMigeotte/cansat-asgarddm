/*
    SD_Logger.cpp
*/

#include "SD_Logger.h"

//#define USE_ASSERTIONS
//#define USE_TIMER

#define DEBUG
#include "DebugCSPU.h"
#include "Timer.h"

#define DBG_LOG_SD 0
#define DBG_FREE_SPACE 0
#define DBG_INIT 0
#define DBG_DIAGNOSTIC 1

bool SD_Logger::storageInitialized = false;

void SD_Logger::setChipSelect( byte ChipSelect ) {
  DASSERT (!storageInitialize);
  chipSelectPinNumber = ChipSelect;
}

SD_Logger::SD_Logger(const byte theChipSelectPinNumber) :
  mySD(),
  chipSelectPinNumber(theChipSelectPinNumber),
  instanceInitialized(false) {}

SD_Logger::~SD_Logger() {
  if (instanceInitialized) storageInitialized = false;
}

bool SD_Logger::initStorage() {
  DPRINTS(DBG_INIT, "init:storageInitialized=");
  DPRINT(DBG_INIT, storageInitialized);
  DPRINTS(DBG_INIT, ", instanceInit=");
  DPRINTLN(DBG_INIT, instanceInitialized);

  if (instanceInitialized) return true;

  if (storageInitialized) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Error: initializing more than 1 SD_Logger instance");
    DASSERT(!storageInitialized);
  }

  bool result = mySD.begin(chipSelectPinNumber);
  if (!result) {
    DPRINTS(DBG_DIAGNOSTIC, "error in mySD.begin CS=");
    DPRINTLN(DBG_DIAGNOSTIC, chipSelectPinNumber);
  }
  instanceInitialized = storageInitialized = result;

  DPRINTS(DBG_INIT, "storageInitialized=");
  DPRINT(DBG_INIT, storageInitialized);
  DPRINTS(DBG_INIT, ", instanceInit=");
  DPRINTLN(DBG_INIT, instanceInitialized);
  return ( result);
}


bool SD_Logger::log(const String& data, const bool addFinalCR) {
  DBG_TIMER("logger::log");
  bool result = false;


  DPRINTS(DBG_LOG_SD, "Logging to file ");
  DPRINTLN(DBG_LOG_SD, fileName());
  File dataFile = mySD.open(fileName().c_str(), FILE_WRITE);

  if (dataFile) {
    DPRINTS(DBG_LOG_SD, "File open: data=");
    DPRINTLN(DBG_LOG_SD, data);
    if (addFinalCR) {
      dataFile.println(data.c_str());
    }
    else {
      dataFile.print(data.c_str());
    }
    DPRINTSLN(DBG_LOG_SD, "Closing");
    dataFile.close();

    DPRINTS(DBG_LOG_SD, "data written: ");
    DPRINTLN(DBG_LOG_SD, data);
    result = true;
  }
  else {
    DPRINTSLN(DBG_DIAGNOSTIC, "error opening file");
  }
  return result;
}



unsigned long SD_Logger::fileSize() {
  if (!fileExists(fileName().c_str())) return 0;

  File theFile = mySD.open(fileName(), FILE_READ);
  uint32_t size = theFile.size();
  theFile.close();
  return size;
}


float SD_Logger::getFreeSpaceInMBytes()
{
  DBG_TIMER("logger::freeSpaceInMBytes");
  DPRINTSLN(DBG_FREE_SPACE, "Checking free space on SD card...");
  float freeMB;
  uint32_t count = mySD.vol()->freeClusterCount();
  uint32_t blocksPerCluster = mySD.vol()->blocksPerCluster();
  count *= blocksPerCluster;
  freeMB = (float)count / 2048.0; // A block is 512 bytes.
  DPRINTS(DBG_FREE_SPACE, "Free space MB: ");
  DPRINTLN(DBG_FREE_SPACE, freeMB);
  return freeMB;
}

bool SD_Logger::fileExists(const char* name)
{
  return mySD.exists(name);
}
