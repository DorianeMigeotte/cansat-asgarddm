/*
 * LineStream.h
 */

#pragma once

#include "Arduino.h"

/** @brief A class used to read from a stream line by line, buffering partial lines
 * 		   to avoid the stream's buffer to overload.
 *
 * 	@par Usage
 * 	  @code
 * 	  	LineStream myLineStream(myInputStream, 150);
 *
 * 	  	// (in setup)
 * 	  	myLineStream.begin();
 *
 * 	  	// (in loop)
 * 	  	// NB: This call returns NULL if no line is received, or a pointer
 * 	  	//     to the buffer containing the line otherwise. Beware that
 * 	  	//     a next call to readLine will possibly reuse the buffer, or return
 * 	  	//     a different one.
 * 	  	const char * str=myLineStream.readLine();
 * 	  	if (str) {
 * 	  		// process string.
 * 	  	}
 * 	  @endcode
 */
class LineStream {
public:

	LineStream();
    ~LineStream();

	/** Initialize the object
	 *  @param theStream The stream from which lines are received. It is expected that
	 *         nobody else will read from this stream. It is expected to be fully initialized
	 *         before the first call to any method.
	 *  @param maxLineLength The size of the longest line to expect (end of string and
	 *  	   end of line excluded).
	 *  @return true if the stream is ready for use, false otherwise.
	 *
	 */
	bool begin(Stream& theStream, unsigned int maxLineLength=100) ;

	/** @brief Receive available characters from the stream.
	 *  It reads until:
	 * 		- until a '\0', '\n', '\n' or any combination of them is received:
	 * 		  in this case a pointer to the string is returned (\0-terminated,
	 * 		  \n and \r excluded)
	 * 		- no additional character is available. in this case the incomplete line
	 * 		  is buffered internally and will be returned at a next call, when completed.
	 *  Beware that a next call to readLine will possibly reuse the buffer, or return
	 *  a different one. This method should be called as often as possible.
	 *  @return Pointer to the received a complete line received ('\0' terminated), or NULL if no
	 *          line available.
	 */
	const char * readLine();
protected:
	/** Read from the stream and buffer until an \0, \n or \r is received or until the
	 *  input queue is empty.
	 *  @return true if the buffer contains a complete line, ready to be processed.
	 */
	bool readFromStream();

private:
	Stream* stream; 		/**< The input stream */
	unsigned int bufferIdx;		/**< Index to the first free position in the buffer */
	unsigned int bufferSize;	/**< The size of the buffer */
	char* buffer;		/**< Pointer to the line buffer */
};


