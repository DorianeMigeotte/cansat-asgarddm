/*
    StorageManager.h
*/

#pragma once
#include "HardwareScanner.h"
#include "SD_Logger.h"
#include "EEPROM_BankWriter.h"

/** @ingroup cansatAsgardCSPU
 *  @brief A class implementing all storage functions common to any Arduino project.
 *  
 *  DESCRIPTION A COMPLETER.
 *  TO IMPROVE: This class should maintain a Logger, not a SD_Logger, in order to support
 *              the use of a FlashLogger as well.
 */
class StorageManager {
  public:
    /** @param theRecordSize The size in bytes of a single record in binary format (always provide by means of
     *         the sizeof() operator, to support various architectures.
     *  @param theCampainDurationInSec The duration of the measurement campaign in seconds, in order for
     *         the storage manager to plan for the usage of th various storage capabilities.
     *  @param theMinStoragePeriodInMsec The minimum time interval between the storage of two records (in msec).
     *         If records are provided before this delay expired, the record will not be stored.
     */
    StorageManager(unsigned int theRecordSize, unsigned int theCampainDurationInSec, unsigned int theMinStoragePeriodInMsec);
    NON_VIRTUAL ~StorageManager() {};

    /** Prepare the StorageManager for work. Call once (in setup()) before calling storeOneRecord().
       If implemented in a subclass, be sure to call inherited::init() before performing additional initialization.
       @return true if the init was successful, false otherwise. This init is considered successful if at least one
       storage destination was successfully initiailized.
    */
    NON_VIRTUAL bool init(HardwareScanner* hwScanner = NULL,
                      const char * fourCharPrefix = "abcd",
                      const byte chipSelectPinNumber = 4,
                      const unsigned int requiredFreeMegs = 100,
                      const EEPROM_BankWriter::EEPROM_Key key = 0x1234,
                      const String& loggerFirstLine="");

    /** Store a record of data. The size is expected to match the size defined when calling init()
       @param binaryData Pointer to a block of recordSize bytes of information, to be stored as-is in memory.
       @param stringData A readable version of the same information, larger but human-readable, to be stored
                         on the SD card, where storage is cheap and unlimited. 
    */
    NON_VIRTUAL void storeOneRecord(const byte* binaryData, const String& stringData, const bool useEEPROM=true);

    /** Store a string on the SD-Card.
     *  @param stringData The string to store.
     */
    NON_VIRTUAL void storeString(const String& stringData);

    /** Get number of records that can be stored in EEPROM
    @return The number of record that can still be accommodated in EEPROM. */
    NON_VIRTUAL unsigned long getNumFreeEEEPROM_Records() const; 
    /** Check whether the logger is operational.
     *  @return true if operational, false otherwise.
     */
    NON_VIRTUAL bool LoggerOperational() const;
    
    /** Call this method regularly to allow for internal maintenance. */ 
    NON_VIRTUAL void doIdle();

    /** Obtain a pointer to the SdFat object (fully initialized and ready to use) if any
     *  @return Pointer to the SdFat object, or null if none available.
     */
     NON_VIRTUAL SdFat* getSdFat(){ return logger.getSdFat();};

     NON_VIRTUAL const String& getSD_FileName() const {return logger.fileName();};
  private:
    /*Actually perform the storage of the record. */

    unsigned int recordSize;        /**< The size of the record in binary format */
    unsigned int campainDuration;   /**< The expected duration of the measurement campain, in seconds. */
    unsigned int minStoragePeriod;  /**< The minimum delay between 2 storage requests in msec. */
    bool initLogger;                /**< True if SD card was succesfully initialized. */
    bool initEEPROM;                /**< True if EEPROM_Bank was successfully initialized */
   
    EEPROM_BankWriter eeprom;       /**< The interface object to the EEPROM */
    SD_Logger logger;               /**< The logger */
};
