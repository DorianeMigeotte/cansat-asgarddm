/* 
 *  This is a second source file using the debugging features.
 */ 

#include "tst_cansat_config.h"

 // By default, turn debugging off. This is just in case the symbol has not previously be defined.
 #ifndef DBG_SRC2 
   #define DBG_SRC2 0
 #endif 

 const int durationOn=100;
 /*
  * Just turn LED on for LED_ON_TIME msec then off for 1 second. 
  */
 void doSomething() 
 {
  DPRINTSLN(DBG_SRC2, "Src 2: Blinking LED once...");
  digitalWrite(LED_BUILTIN, HIGH);
  delay(durationOn);  
  digitalWrite(LED_BUILTIN, LOW);
  delay(durationOn);
  delay(500);
 }


