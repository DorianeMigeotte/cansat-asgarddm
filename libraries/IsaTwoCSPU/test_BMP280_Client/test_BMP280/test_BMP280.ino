#include "IsaTwoRecord.h"
#include "elapsedMillis.h"
#include "BMP280_Client.h"

IsaTwoRecord record;
elapsedMillis elapsed;

const float mySeaLevelPressure = 1018.8;
BMP280_Client bmp;

void setup() {
  // put your setup code here, to run once:
  DINIT(115200);
  Wire.begin();
  if (!bmp.begin(mySeaLevelPressure)) {
    Serial.println("Could not find a valid BMP280 sensor, check wiring!");
    while (1);
  }
}
void loop() {
  // put your main code here, to run repeatedly:
  elapsed = 0;
  bool result = bmp.readData(record);
  if (result == true) {
    Serial.print("Temperature = ");
    Serial.print(record.temperatureBMP);
    Serial.println(" °C");

    Serial << "pressure:" << record.pressure << " hPa" << ENDL;
    Serial << "altitude:" << record.altitude << " m" << ENDL;
  } else {
    Serial << "Error reading data" << ENDL; 
  }
  delay(1000);
}
