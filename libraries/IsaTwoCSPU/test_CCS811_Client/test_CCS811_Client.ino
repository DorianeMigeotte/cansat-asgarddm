#include "CCS811_Client.h"
#include "IsaTwoRecord.h"

IsaTwoRecord record;
CCS811_Client ccs1;

constexpr bool useAnalogPinToWakeSensor = true;
constexpr auto analogPin = A5;
constexpr unsigned long period = 200;

void setup() {
  DINIT(115200);

  bool result;
  if (useAnalogPinToWakeSensor) {
    result = ccs1.beginUsingAnalogWakePin(A5);
  } else {
    result = ccs1.begin();
  }
  if (!result ) {
    Serial << "Error: cannot initialize CCS811_Client" << ENDL;
    while (1) delay(100);
  }
  Serial << "Init OK" << ENDL;
  Serial << "Reading sensor every " << period << " msec." << ENDL;
}

void loop() {
  if (ccs1.readData(record)) {
    Serial << ENDL << millis() << ": got a value: " << record.co2 << " ppm" << ENDL;
  }
  else Serial << '.';
  delay(period);
}
