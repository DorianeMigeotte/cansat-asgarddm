/*
   IsaTwoAcquisitionProcess.h
   A class derived from AcquisitionProcess, which implements the ISATWO-specific
    logic.

    Created on: 20 janv. 2019

*/

#pragma once

#include "AcquisitionProcess.h"
#include "IsaTwoConfig.h"
#include "IsaTwoStorageManager.h"
#include "IsaTwoHW_Scanner.h"
#include "IsaTwoRecord.h"
#include "GPS_Client.h"
#include "TemperatureClient.h"

#ifdef USE_NXP_PRECISION_IMU
#  include "IMU_ClientPrecisionNXP.h"
#else
#  include "IMU_ClientLSM9DS0.h"
#endif

#include "BMP280_Client.h"
#include "CCS811_Client.h"
class IsaTwoAcquisitionProcess: public AcquisitionProcess {
  public:

    IsaTwoAcquisitionProcess();
    virtual ~IsaTwoAcquisitionProcess() {};

    /* Call once and only once before calling run() */
    virtual void init();

    virtual IsaTwoHW_Scanner* getHardwareScanner();
    /** Obtain a pointer to the SdFat object (fully initialized and ready to use) if any
      *  @return Pointer to the SdFat object, or null if none available.
      */
	VIRTUAL SdFat* getSdFat() {
		return storageMgr.getSdFat();
	}
    /** Force the start of the measurement campaign, whatever the actual situation of
     *  the can.
     *  @param msg A message to send to document the reason of the start campaign.
     *  @param value An optional numeric value to document the reason of the start campaign
     */
	void startMeasurementCampaign(const char* msg, float value=0.0f);
    /** Force the stop of the measurement campaign, whatever the actual situation of
     *  the can.  */
	void stopMeasurementCampaign();

  protected:
    virtual void storeDataRecord(const bool campaignStarted);
    virtual void doIdle();
    /** Activate the Imaging controller (includes the clock synchronization) */
    virtual void startImager();

  private:
    virtual void acquireDataRecord();
    virtual void initSpecificProject();

    virtual bool measurementCampaignStarted();


    bool campaignStarted ;
    float lastRecordAltitude;  /**< The altitude present in the previous record (-1 if none). */
    IsaTwoHW_Scanner hwScanner;
    IsaTwoRecord buffer;
#ifdef USE_NXP_PRECISION_IMU
    IMU_ClientPrecisionNXP imu ;
#else
    IMU_ClientLSM9DS0 imu ;
#endif
    BMP280_Client bmp;   
    CCS811_Client co2;
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
    GPS_Client gps;
#endif
    TemperatureClient thermistor;
    IsaTwoStorageManager storageMgr;
    elapsedMillis elapsedSinceSlaveSync;
    float averageSpeed;
};

