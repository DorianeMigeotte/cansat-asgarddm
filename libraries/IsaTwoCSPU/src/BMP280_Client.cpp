/*
 * BMP280_Client.cpp
 */
#include "BMP280_Client.h"

BMP280_Client::BMP280_Client(): bmp() {}

bool BMP280_Client::begin(float theSeaLevelPressureInHPa) {
    if (!bmp.begin()) return false;
     seaLevelPressure=theSeaLevelPressureInHPa;
    return true;
}

bool BMP280_Client::readData(IsaTwoRecord& record){
  record.pressure = bmp.readPressure()/100.0;
  record.altitude = bmp.readAltitude(seaLevelPressure);
  if (isnan(record.altitude)) {
	  record.altitude=0.0;
  }
  record.temperatureBMP = bmp.readTemperature();
  return true;
}
