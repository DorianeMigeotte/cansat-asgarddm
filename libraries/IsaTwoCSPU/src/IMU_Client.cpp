/*
   IMU_Client.cpp
*/

#include "IMU_Client.h"

void IMU_Client::readData(IsaTwoRecord& record) {
	if (numSamplesPerRead < 2) {
		readOneData(record);
	} else readAverage(record);
}

void IMU_Client::readAverage(IsaTwoRecord& record) {

	int32_t a[3], g[3], m[3];

	for (auto i = 0 ; i< 3; i++) {
		m[i] 	  = 0;
		a[i]= 0;
		g[i] = 0;
	}

	for (auto read = 0 ; read< numSamplesPerRead; read++)
	{
		readOneData(record);
		for (auto i = 0 ; i< 3; i++) {
			m[i]+= record.mag[i];
			a[i]+= record.accelRaw[i];
			g[i]+= record.gyroRaw[i];
		}
	}

	for (auto i = 0 ; i< 3; i++) {
		record.mag[i] 	  = m[i]/numSamplesPerRead;
		record.accelRaw[i]= a[i]/numSamplesPerRead;
		record.gyroRaw[i] = g[i]/numSamplesPerRead;
	}
}
